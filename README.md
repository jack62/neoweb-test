NEOWEB PROJECT


Objectif - Réalisation du test pour l'affichage des horaires d'un restaurant 



Information Installation  

        1.Cloner le Repo sur votre machine/en local
        2.Effectué la commande Composer install pour réinstaller les dépendances 
        3.Crée la base de donnée avec php bin/console do:da:cr (doctrine:databases:create)
        4.Installer la base de donnée avec un php bin/console do:mi:mi (doctrine:migrations:migrate)
        5.Lancer l'application avec php bin/console se:ru (server:run) il vous indiquera le lien pour visualiser le site 

Amélioration : 

        1. Possible de mettre les valeurs des horaire en base de donnée pour les affichées
        2. Création d'un tableau avec les horaires 
        3. Crée une seconde table ou on depose les horaire d'ouverture/Fermeture avec l'id du restaurant et dans restaurant on fait une relation entre les deux table pour les afficher.