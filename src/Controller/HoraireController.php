<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HoraireController extends AbstractController
{


    // Cette function sert à crée la route ou nous y trouverons nos restaurant par nom , au clique nous aurons les horaires du restaurant 
    // nous pourrions aussi faire en sorte de mettre les horaire dans un tableau en base de donnée pour effectué une recherche plus rapide

    /**
     * @Route("/", name="home")
     */
    public function indexHoraire(RestaurantRepository $restoRepo)
    {
        // Appel de l'entité Restaurant pour allez y cherche le nom et afficher les horaire selon le restaurant choissie 
        $getResto = new Restaurant();
        $getResto = $restoRepo->findAll();
        return $this->render('horaire/index.html.twig',[
            'restaurant' => $getResto,
        ]);
    }

    // cette function permet l'affichage des horaires selon le choix effectué sur notre page Home
    /**
     * @Route("/restaurant/{id}", name="horaire")
     */
    public function restaurantHoraire(RestaurantRepository $restoRepo,$id)
    {

         // Appel de l'entité Restaurant pour allez y cherche le nom et afficher les horaire selon le restaurant choissie 
         $getResto = new Restaurant();
         $getResto = $restoRepo->find($id);

        return $this->render('horaire/horaire.html.twig',[
            'restoId' => $id,  // Utiliser pour les conditions pour selectionner le bon restaurant 
            'restoName' => $getResto, // On affiche le nom du restaurant pour le tableau selon l'id selectionner
        ]);
    }
}
